# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from guardian import GuardState

from ..masterswitch.states import *
from ..watchdog.states import *
from ..isolation.states import *

from ..isolation import const as iso_const
from ..watchdog import const as watchdog_const

from ..watchdog import util as watchdog_util
from ..masterswitch import util as masterswitch_util
from ..isolation import util as isolation_util


class INIT(GuardState):
    def main(self):
        log('determining how to get to defined state...')

        if watchdog_util.is_watchdog_tripped():
            isolation_util.clear_iso_filters()

            watchdog_state = watchdog_util.read_watchdog_state()
            if watchdog_state == watchdog_const.WATCHDOG_DEISOLATING_STATE:
                return 'WATCHDOG_TRIPPED_DEISOLATING'
            if watchdog_state == watchdog_const.WATCHDOG_DAMPING_STATE:
                return 'WATCHDOG_TRIPPED_DAMPING'
            if watchdog_state == watchdog_const.WATCHDOG_FULL_SHUTDOWN_STATE:
                return 'WATCHDOG_TRIPPED_FULL_SHUTDOWN'

        if masterswitch_util.is_masterswitch_off():
            isolation_util.clear_iso_filters()
            return 'MASTERSWITCH_OFF'

        for control_level in iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS']:
            error_message = isolation_util.check_if_isolation_loops_on(control_level)
            if not error_message:
                return control_level+'_ISOLATED'

        isolation_util.clear_iso_filters()
        return 'READY'
